# Problem - 1

The solution to this task is located at [TASK_1](./TASK_1)

# Problem - 2

The solution to this task is located at [TASK_2](./TASK_2/TaskQuest)

# Problem - 3

The solution to this task is located at [TASK_2](./TASK_3)

# Videos

https://youtu.be/M1NLB7dgJmE

# Previous Project Link

https://youtu.be/vnaYnHySZpo

# Mysql

https://youtu.be/8dCsIzir5Bo
