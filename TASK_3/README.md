### How to manage Perodic tasks

To schedule periodic tasks, one of the popular choices is to use a cron job scheduler 
in a Linux environment. Cron is a time-based job scheduler in Unix/Linux systems that 
allows users to schedule jobs or commands to run automatically at specified times or 
intervals. It is reliable enough for small to medium-scale projects and can be easily 
set up to run periodic tasks such as downloading a list of ISINs every 24 hours.

However, if the project is expected to scale to handle large volumes of periodic tasks,
Cron may not be the best option. Cron is not designed for handling a large number of 
jobs and has limitations in terms of scalability and handling failures. 
In such cases, we can consider using more advanced job scheduling systems 
like Apache Airflow, Celery, or AWS Batch, which are better suited for 
handling large-scale periodic jobs.

### Django and Flask

Flask and Django are both popular web frameworks used in Python for developing 
web applications. Flask is a micro web framework that is lightweight, flexible, 
and easy to learn. It is suitable for building small to medium-sized web applications 
that require minimal features and functionalities. Flask is best used for rapid prototyping
or building small to medium-sized web applications, where the focus is on simplicity,
modularity, and ease of use.

Django, on the other hand, is a full-stack web framework that is more comprehensive
and feature-rich. It provides built-in components for handling common web development 
tasks such as authentication, routing, database modeling, and templating. Django is 
suitable for building large-scale web applications that require complex features 
and functionalities. Django is best used for developing large-scale web applications
where the focus is on security, scalability, and maintainability.

In summary, Flask is a good choice for building small to medium-sized web applications
that require simplicity and modularity, while Django is more suitable for developing 
large-scale web applications that require comprehensive features and functionalities.

### Extra Opinion
To blow your mind, I can provide an example project that incorporates all these good 
software engineering practices. 
For instance, let's consider a web application that allows users to create, view, and edit notes.
 Here are some of the features that this project can include:

Modular codebase: The codebase can be broken down into smaller modules, such as models, views,
 templates, and static files, to make it more manageable and maintainable.
Clear and concise documentation: The codebase can include comments in the code and
 a README file that provides information about the project's functionality, usage, and dependencies.
Normalized database schema: The database schema can be designed to have normalized
 tables that reduce data redundancy, improve data integrity, and optimize queries.
requirements.txt: The project can maintain a requirements.txt file that lists all 
the dependencies and their versions, ensuring that the code can be run consistently
 across different environments.